#![feature(slice_patterns)]

mod amazon;

extern crate serde;
#[macro_use]
extern crate serde_derive;

extern crate chrono;
use chrono::Utc;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Tide {
    instant: u64,
    height: f64,
    #[serde(rename = "highOrLow")]
    high_or_low: String,
    dow: String,    // E.g., "Sunday"
    date: String,   // E.g., "2017-12-25"
    time12: String, // E.g., "2:15 AM"
    time24: String, // E.g., "02:15"
    source: String, // E.g., "EASYTIDE"
}

// Tides may be from mixed sources, with slightly different times for the same tide.
// We guard against confusing reporting by ignoring tides that occur within a short period of another.
// For more discussion: https://gitlab.com/d6y/brightontide-post/snippets/1752912
fn single_origin(tides: Vec<Tide>) -> Vec<Tide> {
    // We want the first element, or anything with a time difference of 3 hours or more
    let three_hours_ms = 3 * 60 * 60 * 1000;
    let keep_plausible_changes =
        |(i, t): &(usize, Tide)| i == &0 || t.instant - tides[i - 1].instant > three_hours_ms;
    let plausible_tides: Vec<Tide> = tides
        .iter()
        .cloned()
        .enumerate()
        .filter(keep_plausible_changes)
        .map(|(_, t)| t)
        .collect();
    plausible_tides
}

fn main() {
    let now = Utc::now();

    match amazon::fetch_tides::<Tide>(now) {
        Ok(mut tides) => {
            tides.sort_by_key(|tide| tide.instant);
            for tide in single_origin(tides).iter().take(3) {
                println!("{:?}", tide);
            }
        }
        Err(error) => {
            println!("Error: {:?}", error);
        }
    }
}

#[cfg(test)]
mod tests {

    use super::single_origin;
    use super::Tide;

    #[test]
    fn single_origin_when_all_same() {
        let input: Vec<Tide> = vec![tide(13, 00), tide(19, 00)];
        let output: Vec<Tide> = single_origin(input);
        assert_eq!(output.len(), 2);
    }

    #[test]
    fn single_origin_when_mixed() {
        let input: Vec<Tide> = vec![tide(1, 00), tide(1, 01), tide(13, 00)];
        let output: Vec<Tide> = single_origin(input);
        assert_eq!(output.len(), 2);
    }

    #[test]
    fn single_origin_when_mixed_and_same() {
        let input: Vec<Tide> = vec![tide(1, 00), tide(1, 00), tide(13, 00)];
        let output: Vec<Tide> = single_origin(input);
        assert_eq!(output.len(), 2);
    }

    fn tide(hour: u64, minute: u64) -> Tide {
        let instant: u64 = ((hour * 60) + minute) * 60 * 1000;
        Tide {
            instant: instant,
            // remainder of these fields arbitrary
            source: "VB".to_string(),
            height: 1.0,
            high_or_low: "low".to_string(),
            dow: "Monday".to_string(),
            date: "2018-01-01".to_string(),
            time12: "10am".to_string(),
            time24: "10:00".to_string(),
        }
    }

}
