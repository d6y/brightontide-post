extern crate chrono;
extern crate rusoto_core;
extern crate rusoto_dynamodb;
extern crate serde;
extern crate serde_dynamodb;

use std::collections::HashMap;
use std::default::Default;

use self::chrono::{DateTime, Utc};
use self::rusoto_core::Region;
use self::rusoto_dynamodb::{AttributeValue, DynamoDb, DynamoDbClient, ScanError, ScanInput};
use serde::de::DeserializeOwned;

pub fn fetch_tides<T: DeserializeOwned>(when: DateTime<Utc>) -> Result<Vec<T>, ScanError> {
    let scan = ScanInput {
        table_name: String::from("brighton-tide"),
        filter_expression: Some(String::from("instant >= :when and highOrLow = :highOrLow")),
        expression_attribute_values: Some(dynamo_query_placeholders(when)),
        ..Default::default()
    };

    let client = DynamoDbClient::new(Region::EuWest1);

    // TODO: remove the sync here and have a future result?
    client.scan(scan).sync().map(|scanout| {
        scanout
            .items
            .unwrap_or_else(|| vec![])
            .into_iter()
            .map(|item| serde_dynamodb::from_hashmap(item).unwrap())
            .collect()
    })
}

fn dynamo_query_placeholders(when: DateTime<Utc>) -> HashMap<String, AttributeValue> {
    let params: HashMap<String, AttributeValue> = [
        (
            String::from(":highOrLow"),
            AttributeValue {
                s: Some(String::from("low")),
                ..Default::default()
            },
        ),
        (
            String::from(":when"),
            AttributeValue {
                n: Some(when.timestamp_millis().to_string()),
                ..Default::default()
            },
        ),
    ].iter()
        .cloned()
        .collect();

    params
}
